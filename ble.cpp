#include "ble.h"
#include "AltSoftSerial.h"
#include "simple_ble.h"
#include "scripting.h"

#include <Arduino.h>



// AltSoft lib uses these RX and TX pins for communication but it doesn't
// realy nead them to be defined here. This is just for reference.
#define RX_PIN 8
#define TX_PIN 9
#define RX_ENABLE_PIN 10
#define MODULE_RESET_PIN 11


static AltSoftSerial altSerial;


static SimpleBLE ble(
    [](bool state) { digitalWrite(RX_ENABLE_PIN, state ? HIGH : LOW); },
    [](bool state) { digitalWrite(MODULE_RESET_PIN, state ? HIGH : LOW); },
    [](char c) { return altSerial.write(c) > 0; },
    [](char *c)
    {
        bool availableChars = altSerial.available() > 0;

        *c = availableChars ? altSerial.read() : *c ;

        return availableChars;
    },
    [](void) { return (uint32_t)millis(); },
    [](uint32_t ms) { delay(ms); },
    [](const char *dbg) { Serial.print(dbg); }
    //NULL
);


int dfuService = -1;
int dfuChar = -1;

int outVarService = -1;
int outVarChar[4] = {-1, -1, -1, -1};
int outVarChangeChar[4] = {-1, -1, -1, -1};


uint8_t msd[3] = {0x7D, 0x02, 0x00};


void initBle(void)
{
    pinMode(RX_ENABLE_PIN, OUTPUT);
    pinMode(MODULE_RESET_PIN, OUTPUT);

    altSerial.begin(9600);

    ble.begin();

    Serial.println("Reseting the device");
    ble.softRestart();
    delay(10);

    Serial.println("Setting TX power");
    ble.setTxPower(SimpleBLE::POW_0DBM);
    delay(100);

    Serial.println("Adding the service");
    dfuService = ble.addService(0xA0);
    Serial.print("Added service: ");
    Serial.println(dfuService);
    delay(100);

    if( dfuService >= 0 )
    {
        dfuChar = ble.addChar(
            dfuService,
            200,
            SimpleBLE::READ | SimpleBLE::WRITE | SimpleBLE::NOTIFY);

        Serial.print("Added characteristic: ");
        Serial.println(dfuChar);
        delay(100);
    }

    Serial.println("Adding the service");
    outVarService = ble.addService(0xB0);
    Serial.print("Added service: ");
    Serial.println(outVarService);
    delay(100);

    if( outVarService >= 0 )
    {
        for(int i = 0; i < sizeof(outVarChar); i++)
        {
            outVarChar[i] = ble.addChar(
                outVarService,
                4,
                SimpleBLE::READ | SimpleBLE::WRITE | SimpleBLE::NOTIFY);

            outVarChangeChar[i] = ble.addChar(
                outVarService,
                1,
                SimpleBLE::READ | SimpleBLE::WRITE | SimpleBLE::NOTIFY);

            Serial.print("Added characteristic: ");
            Serial.println(outVarChar[i]);
            delay(100);
        }
    }

    const char devName[] = "SimpleBLE example";
    ble.setAdvPayload(SimpleBLE::COMPLETE_LOCAL_NAME, (uint8_t*)devName, sizeof(devName)-1);
    ble.setAdvPayload(SimpleBLE::MANUFACTURER_SPECIFIC_DATA, msd, sizeof(msd));
    delay(100);

    Serial.println("Starting advertisement.");
    ble.startAdvertisement(100, SIMPLEBLE_INFINITE_ADVERTISEMENT_DURATION, true);
    delay(100);
}

void processBle(void)
{
    bool isUpdated = false;

    for(int i = 0; i < 4; i++)
    {
        int32_t val;
        if( getVars()->getVar(i, &val, &isUpdated) )
        {
            if( isUpdated )
            {
                const uint8_t updated = 1;
                ble.writeChar(outVarService, outVarChar[i], (uint8_t*)&val, sizeof(val));
                ble.writeChar(outVarService, outVarChangeChar[i], (uint8_t*)&updated, sizeof(updated));
                msd[2] |= 1 << i;
            }
        }
    }
    ble.setAdvPayload(SimpleBLE::MANUFACTURER_SPECIFIC_DATA, msd, sizeof(msd));
}
