#ifndef __TIMEOUT_H__
#define __TIMEOUT_H__


#include <stdio.h>
#include <stdint.h>


typedef uint32_t (MillisType)(void);


class Timeout
{
public:

    Timeout(uint32_t timeout) : timeout(timeout)
    {
        _millis = init(NULL);

        restart();
    }

    // Call this before any other calls.
    static MillisType **init(MillisType *millisF)
    {
        static MillisType *initInternalMillis = NULL;

        if( millisF != NULL )
        {
            initInternalMillis = millisF;
        }

        return &initInternalMillis;
    }

    inline void restart() { startTime = privMillis(); }
    inline void restart(uint32_t newTimeout)
    {
        timeout = newTimeout;
        restart();
    }

    inline uint32_t passed(void) { return privMillis() - startTime; }

    inline int32_t remaining(void)
    {
        uint32_t passedTime = passed();
        int32_t retval = timeout - passedTime ;

        if( timeout >= passedTime )
        {
            retval = retval < 0 ? INT32_MAX : retval ;
        }
        else
        {
            retval = retval > 0 ? INT32_MIN : retval ;
        }

        return retval;
    }

    inline bool expired(void) { return passed() >= timeout ; }

    inline bool notExpired(void) { return !expired(); }

private:

    inline uint32_t privMillis(void)
    {
        return *_millis ? (*_millis)() : 0 ;
    }

    MillisType **_millis;

    uint32_t startTime;
    uint32_t timeout;
};


#endif//__TIMEOUT_H__
