#include "cmd_interface.h"
#include "xutility.h"
#include "timeout.h"

#include <string.h>


uint32_t CmdInterface::removeBackspace(char *lineBuff, uint32_t lineBuffLen)
{
    uint32_t retval = lineBuffLen;

    for(uint32_t i = 0; lineBuff[i]; i++)
    {
        if( lineBuff[i] == '\b' )
        {
            uint32_t numB = 1;
            while( lineBuff[i+numB] == '\b' )
            {
                numB++;
            }

            uint32_t reduceNum;
            if( (int32_t)(i-numB) < 0 )
            {
                reduceNum = numB*2 + (i-numB);
                lineBuff[0] = '\0';
            }
            else
            {
                reduceNum = numB*2;
                lineBuff[i-numB] = '\0';
            }

            for(uint32_t j = 0, k = 0;
                j < lineBuffLen;
                j++, k++
            )
            {
                // When replace string is smaller than search string we cant overflow
                // buffer.
                lineBuff[k] = lineBuff[j];

                if( lineBuff[j] == '\0' )
                {
                    j += reduceNum - 1;
                    k += 0 - 1;
                }
            }
            lineBuffLen -= reduceNum;
            lineBuff[lineBuffLen] = '\0';
            i = -1;
            retval = lineBuffLen;
        }
    }

    return retval;
}

CmdInterface::RetCode CmdInterface::executeCommand(uint32_t timeout)
{
    CmdInterface::RetCode retval = {CmdInterface::SUCCESS};
    debug("> ");

do{

    size_t lineBuffLen;

    struct BackspaceContext
    {
        PutterChar *putc;
    } context = {
        putc
    };

    CharHandler *backspaceChecker = [](char c, void* handlerContext)
    {
        BackspaceContext *pContext = (BackspaceContext*)handlerContext;

        if( c == '\b' )
        {
            pContext->putc(' ');
            pContext->putc('\b');
        }
    };

    if( !(lineBuffLen = atProc.getLine(lineBuff, maxLineLen, timeout, backspaceChecker, &context)) )
    {
        debug("Command interface timed out\r\n");
        putc('\r');
        putc('\n');
        retval = {CmdInterface::TIMEOUT_ERROR};
        break;
    }

    removeBackspace(lineBuff, lineBuffLen);

    retval = getCommand();

}while(0);

    return retval;
}

CmdInterface::RetCode CmdInterface::getCommand(void)
{
    CmdInterface::RetCode retval = {CmdInterface::WRONG_COMMAND};

    char *token = strtok(lineBuff, " ");

    if( token != NULL ) {
        uint32_t numOfCmdDescriptors = cmdListSize/sizeof(CommandDescriptor);
        for(uint32_t i = 0; i < numOfCmdDescriptors; i++)
        {
            size_t cmdCommandLen = strlen(cmdList[i].cmdString);
            if ( strncmp(token, cmdList[i].cmdString, cmdCommandLen) == 0 )
            {
                size_t tokenCommandLen = strlen(token);
                xTrim(token, tokenCommandLen, tokenCommandLen+1);
                tokenCommandLen = strlen(token);
                if( tokenCommandLen != cmdCommandLen )
                {
                    break;
                }
                token = strtok(NULL, " ");
                char *argList[cmdList[i].cmdMaxNumArg];
                int j;
                uint32_t numArg = 0;
                for(j = 0; j < cmdList[i].cmdMaxNumArg; j++)
                {
                    if( token != NULL )
                    {
                        tokenCommandLen = strlen(token);
                        xTrim(token, tokenCommandLen, tokenCommandLen+1);
                        argList[j] = token;
                        numArg++;
                        token = strtok(NULL, " ");
                    }
                }
                cmdList[i].cmdHandler(argList, numArg);
                retval.commandNum = i;
                break;
            }
        }
    }
    return retval;
}

void CmdInterface::debug(const char *out)
{
    if( dbgOut )
    {
        dbgOut(out);
    }
}