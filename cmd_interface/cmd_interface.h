#ifndef __CMD_INTERFACE_H__
#define __CMD_INTERFACE_H__

#include "./at_process/at_process.h"

#include <stdint.h>
#include <stddef.h>

#define MAX_LINE_LEN_B                      (80+1)

struct CommandDescriptor
{
    bool (*cmdHandler)(char**, uint32_t);
    uint8_t cmdMaxNumArg;
    const char *cmdString;
};

typedef void (Delay)(uint32_t);
typedef uint32_t (Millis)(void);

typedef bool (PutterChar)(char);
typedef bool (CharGetter)(char *);

typedef void (DebugOutput)(const char *);
typedef void (CharHandler)(char, void*);

typedef void (DebugOutput)(const char *);

class CmdInterface
{

public:

    enum Errors : int
    {
        TIMEOUT_ERROR = -2,
        WRONG_COMMAND,
        SUCCESS
    };

    union RetCode
    {
        Errors err;
        int32_t commandNum;
    };

    CmdInterface(Delay *delay,
                Millis *millis,
                PutterChar *putc,
                CharGetter *getc,
                CommandDescriptor *cmdList,
                uint32_t cmdListSize,
                char *lineBuff,
                uint32_t maxLineLen,
                DebugOutput *dbgOut = NULL) :
                delay(delay),
                millis(millis),
                putc(putc),
                getc(getc),
                cmdList(cmdList),
                cmdListSize(cmdListSize),
                lineBuff(lineBuff),
                maxLineLen(maxLineLen),
                dbgOut(dbgOut),
                atProc(putc, getc, delay, millis, dbgOut)
                {
                    lineBuff[maxLineLen-1] = '\0';
                }

    RetCode getCommand(void);
    RetCode executeCommand(uint32_t timeout);
    uint32_t removeBackspace(char *lineBuff, uint32_t lineBuffLen);

private:

    void debug(const char *out);

    Delay *delay;
    Millis *millis;

    PutterChar *putc;
    CharGetter *getc;

    DebugOutput *dbgOut;

    AtProcess atProc;

    CommandDescriptor *cmdList;
    uint32_t cmdListSize;
    char *lineBuff;
    uint32_t maxLineLen;
};



#endif //__CMD_INTERFACE_H__
