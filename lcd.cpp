#include "Arduino.h"
#include "Wire.h"
#include "LiquidCrystal_I2C.h"


LiquidCrystal_I2C lcd(0x27, 16, 2);  // Configure LiquidCrystal_I2C library with 0x27 address, 16 columns and 2 rows


void initLcd(void)
{
    lcd.init();                        // Initialize I2C LCD module

    lcd.backlight();                   // Turn backlight ON

    lcd.setCursor(0, 0);               // Go to column 0, row 0
    lcd.print("Hello, world!");
    lcd.setCursor(0, 0);               // Go to column 0, row 0
}

LiquidCrystal_I2C *getLcd(void)
{
    return &lcd;
}
