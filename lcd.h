#ifndef __LCD_H__
#define __LCD_H__

#include "LiquidCrystal_I2C.h"



void initLcd(void);
LiquidCrystal_I2C *getLcd(void);

#endif//__LCD_H__
