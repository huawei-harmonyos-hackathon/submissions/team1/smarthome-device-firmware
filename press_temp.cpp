#include "press_temp.h"

#include "Adafruit_BMP280.h"


static Adafruit_BMP280 bmp; // use I2C interface

void initPressTemp(void)
{
    if (!bmp.begin(BMP280_ADDRESS_ALT)) {
        Serial.println(F("Could not find a valid BMP280 sensor, check wiring or "
                        "try a different address!"));
        while (1) delay(10);
    }

    /* Default settings from datasheet. */
    bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                    Adafruit_BMP280::SAMPLING_X16,     /* Temp. oversampling */
                    Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                    Adafruit_BMP280::FILTER_OFF,      /* Filtering. */
                    Adafruit_BMP280::STANDBY_MS_1); /* Standby time. */
}

bool detectRoberry(void)
{

    static float prevPress = 0.;
    static float acc = 0.;

    float press = bmp.readPressure();

    acc += press - prevPress;
    acc *= 0.9;
    prevPress = press;

    bool retval = acc >= 5.0 ? true : false ;

    //Serial.println(acc);

    //delay(16);

    return retval;
}

float getTemperature(void)
{
    return bmp.readTemperature();
}
