#include <Arduino.h>

#include "scripting.h"
#include "cmd_interface.h"



static AtomVariable vars[] = {
    {
        "out0",
        0,
        false
    },
    {
        "out1",
        0,
        false
    },
    {
        "out2",
        0,
        false
    },
    {
        "out3",
        0,
        false
    },
    {
        "var0",
        0,
        false
    },
    {
        "var1",
        0,
        false
    },
    {
        "var2",
        0,
        false
    },
    {
        "var3",
        0,
        false
    },
    {
        "var4",
        0,
        false
    }
};



static ScriptVariable varOrganiser(vars);


static char lineBuff[MAX_LINE_LEN_B];


static bool processPinModeCmd(char** argList, uint32_t numArg);
static bool processWritePinCmd(char** argList, uint32_t numArg);
static bool processReadPinCmd(char** argList, uint32_t numArg);
static bool processHelpCmd(char** argList, uint32_t numArg);

static CommandDescriptor cmdList[] =
{
    {
        processPinModeCmd,
        3,
        "pinmode"
    },
    {
        processWritePinCmd,
        3,
        "writepin"
    },
    {
        processReadPinCmd,
        4,
        "readpin"
    },
    {
        processHelpCmd,
        0,
        "help"
    }
};


static CmdInterface cmdInterface(
    delay,
    millis,
    [](char c) { return Serial.write(c) > 0; },
    [](char *c)
    {
        bool availableChars = Serial.available() > 0;

        *c = availableChars ? Serial.read() : *c ;

        return availableChars;
    },
    cmdList,
    sizeof(cmdList),
    lineBuff,
    sizeof(lineBuff),
    [](const char *dbg) { Serial.print(dbg); }
);



int8_t ScriptVariable::getVarIndex(const char *varName)
{
    const uint8_t outVarNum = 4, varNum = 5;

    int8_t index = -1;

do{
    if( *varName == 'o' )
    {
        index = 0;
    }
    else if( *varName == 'v' )
    {
        index = outVarNum;
    }
    else
    {
        break;
    }

    index += atoi(varName[3]);
}while(0);

    return index;
}

bool ScriptVariable::setVar(const char *varName, int32_t val)
{
    return setVar(getVarIndex(varName), val);
}
bool ScriptVariable::setVar(int8_t varIndex, int32_t val)
{
    bool retval = false;

    if( varIndex >= 0 )
    {
        varSpace[varIndex].varVal = val;
        varSpace[varIndex].updated = true;
        retval = true;
    }

    return retval;
}

bool ScriptVariable::getVar(const char *varName, int32_t *val, bool *isUpdated)
{
    return getVar(getVarIndex(varName), val, isUpdated);
}
bool ScriptVariable::getVar(int8_t varIndex, int32_t *val, bool *isUpdated)
{
    bool retval = false;

    if( varIndex >= 0 )
    {
        *val = varSpace[varIndex].varVal;
        varSpace[varIndex].updated = false;
        retval = true;
    }

    return retval;
}

bool ScriptVariable::varUpdated(const char *varName, bool *isUpdated)
{
    return varUpdated(getVarIndex(varName), isUpdated);
}
bool ScriptVariable::varUpdated(int8_t varIndex, bool *isUpdated)
{
    bool retval = false;

    if( varIndex >= 0 )
    {
        *isUpdated = varSpace[varIndex].updated;
        retval = false;
    }

    return retval;
}


ScriptVariable *getVars(void)
{
    return &varOrganiser;
}


void uartCommHandler(const void* argument)
{
    cmdInterface.executeCommand(100);
}



static bool processPinModeCmd(char** argList, uint32_t numArg)
{
    bool retval = false;

    int8_t pin = -1;
    uint8_t mode = INPUT;

do{
    uint8_t i;
    for(i = 0; i < numArg; i++)
    {
        if( strcmp(argList[i], "--pin") == 0 )
        {
            pin = atoi(argList[i+1]);
            break;
        }
    }
    if( i >= numArg )
    {
        break;
    }

    for(i = 0; i < numArg; i++)
    {
        if( strcmp(argList[i], "--output") == 0 )
        {
            mode = OUTPUT;
            break;
        }
    }
    if( i >= numArg )
    {
        break;
    }

    pinMode(pin, mode);

    retval = true;

}while(0);

    return retval;
}

static bool processWritePinCmd(char** argList, uint32_t numArg)
{
    bool retval = false;

    int8_t pin = -1;
    uint8_t val;

do{

    uint8_t i;
    for(i = 0; i < numArg; i++)
    {
        if( strcmp(argList[i], "--pin") == 0 )
        {
            pin = atoi(argList[i+1]);
            break;
        }
    }
    if( i >= numArg )
    {
        break;
    }

    for(i = 0; i < numArg; i++)
    {
        if( strcmp(argList[i], "--high") == 0 )
        {
            val = HIGH;
            break;
        }
        else if( strcmp(argList[i], "--low") == 0 )
        {
            val = LOW;
            break;
        }
    }
    if( i >= numArg )
    {
        break;
    }

    digitalWrite(pin, val);

    retval = true;

}while(0);

    return retval;
}

static bool processReadPinCmd(char** argList, uint32_t numArg)
{
    bool retval = false;

    int8_t pin = -1;
    char *saveVarName = NULL;

do{

    uint8_t i;
    for(i = 0; i < numArg; i++)
    {
        if( strcmp(argList[i], "--pin") == 0 )
        {
            pin = atoi(argList[i+1]);
            break;
        }
    }
    if( i >= numArg )
    {
        break;
    }

    for(i = 0; i < numArg; i++)
    {
        if( strcmp(argList[i], "--var") == 0 )
        {
            saveVarName = argList[i+1];
            break;
        }
    }
    if( i >= numArg )
    {
        break;
    }

    if( !varOrganiser.setVar(saveVarName, digitalRead(pin)) )
    {
        break;
    }

    retval = true;

}while(0);

    return retval;
}

static bool processHelpCmd(char** argList, uint32_t numArg)
{
    bool retval = true;
    Serial.println("");
    Serial.println("COMMANDS");
    Serial.println("NAME\t\t\tARGUMENTS");
    Serial.println("help\t\t\t0");
    Serial.println("");
    return retval;
}
