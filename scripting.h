#ifndef __SCRIPTING_H__
#define __SCRIPTING_H__


struct AtomVariable
{
    char *varName;
    int32_t varVal;
    bool updated;
};

class ScriptVariable
{
public:

    ScriptVariable(AtomVariable *varSpace) : varSpace(varSpace) {}

    int8_t getVarIndex(const char *varName);

    bool setVar(const char *varName, int32_t val);
    bool setVar(int8_t varIndex, int32_t val);
    bool getVar(const char *varName, int32_t *val, bool *isUpdated);
    bool getVar(int8_t varIndex, int32_t *val, bool *isUpdated);

    bool varUpdated(const char *varName, bool *isUpdated);
    bool varUpdated(int8_t varIndex, bool *isUpdated);

    AtomVariable *varSpace;
};

ScriptVariable *getVars(void);
void uartCommHandler(const void* argument);

#endif // __SCRIPTING_H__
