#include <Arduino.h>
#include "cmd_interface.h"
#include "scripting.h"
#include "ble.h"
#include "press_temp.h"
#include "lcd.h"

#include "timeout.h"




static Timeout secondTimeout(10000);


void setup()
{
    Timeout::init(millis);

    Serial.begin(115200);

    Serial.println("Example started");

    initBle();
    initPressTemp();
    initLcd();
}

void loop()
{
    uartCommHandler(NULL);
#if 0
    int32_t availableData = 100;
    availableData = ble.checkChar(exampleService, exampleChar);

    if( availableData != 0 )
    {
        Serial.print("Read length: ");
        Serial.print(availableData);

        availableData = abs(availableData);
        uint8_t recvData[availableData];

        ble.readChar(exampleService, exampleChar, recvData, availableData);

        Serial.print(", data: ");
        for(int i = 0; i < availableData; i++)
        {
            Serial.print(recvData[i], HEX);
            Serial.print(" ");
        }
        Serial.println("");
    }

    if( secondTimeout.expired() )
    {
        secondTimeout.restart();

        uint32_t currMillis = millis();

        ble.writeChar(exampleService, exampleChar,
                      (uint8_t*)&currMillis, sizeof(currMillis));

    }

    delay(30);
#endif
}

